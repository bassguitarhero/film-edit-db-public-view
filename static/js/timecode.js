function timeToSeconds(t) {
	var tc = t.split(':');
	return parseInt(tc[0])*3600 + parseInt(tc[1])*60 + parseInt(tc[2]);
}

function tcDuration(tcin, tcout) {
	function z(n){return (n<10?'0':'') + n;}
	var duration = timeToSeconds(tcout) - timeToSeconds(tcin);
	var hoursmins = Math.floor(duration / 60);
	return z(Math.floor(hoursmins/60)) + ':' + z(hoursmins % 60) + ':' + z(duration % 60);
}

// Run this function every time a film_tc_out cell is changed
function film_tc_Duration() {
	if (document.getElementById("film_tc_in").value == '') {var film_tc_in = '00:00:00';} else { var film_tc_in = document.getElementById("film_tc_in").value;}
	if (document.getElementById("film_tc_out").value == '') {var film_tc_out = '00:00:00';} else { var film_tc_out = document.getElementById("film_tc_out").value;}
	document.getElementById("film_tc_duration").value = tcDuration(film_tc_in, film_tc_out);
}

// Run this function every time a src_tc_out cell is changed
function src_tc_Duration() {
	if (document.getElementById("src_tc_in").value == '') {var src_tc_in = '00:00:00';} else { var src_tc_in = document.getElementById("src_tc_in").value;}
	if (document.getElementById("src_tc_out").value == '') {var src_tc_out = '00:00:00';} else { var src_tc_out = document.getElementById("src_tc_out").value;}
	document.getElementById("src_tc_duration").value = tcDuration(src_tc_in, src_tc_out);
}

// Run this function every time a src_wd_out cell is changed
function src_wd_Duration() {
	if (document.getElementById("src_wd_in").value == '') {var src_wd_in = '00:00:00';} else { var src_wd_in = document.getElementById("src_wd_in").value;}
	if (document.getElementById("src_wd_out").value == '') {var src_wd_out = '00:00:00';} else { var src_wd_out = document.getElementById("src_wd_out").value;}
	document.getElementById("src_wd_duration").value = tcDuration(src_wd_in, src_wd_out);
}

// This determines the cost of the asset after the user selects either timecode or windowdub as their duration
function calculateCost() {
	var asset_cost = document.getElementById("asset_cost").value;
	var duration = document.getElementById("tc_or_wd").value;
	var cost_rate = document.getElementById("cost_rate").value;
	if (document.getElementById("cost_rate").value == 'Second') {
		var cost = parseFloat(asset_cost) * parseFloat(duration);
	} else if (document.getElementById("cost_rate").value == 'Minute') {
		var cost = parseFloat(parseFloat(asset_cost) * parseFloat(duration)/60).toFixed(2);
	} else if (document.getElementById("cost_rate").value == 'Use') {
		var cost = asset_cost;
	}
	document.getElementById("total_cost").value = cost;
}
function toggle_visibility(id) {
       var e = document.getElementById(id);
       if(e.style.display == 'block')
          e.style.display = 'none';
       else
          e.style.display = 'block';
    }

function revealDetails() {
       var details = document.getElementById('details');
       var instances = document.getElementById('instances');
       var licensing = document.getElementById('licensing');
       var notes = document.getElementById('notes');
       details.style.display = 'block';
       instances.style.display = 'none';
       licensing.style.display = 'none';
       notes.style.display = 'none';
    }

function revealInstances() {
       var details = document.getElementById('details');
       var instances = document.getElementById('instances');
       var licensing = document.getElementById('licensing');
       var notes = document.getElementById('notes');
       details.style.display = 'none';
       instances.style.display = 'block';
       licensing.style.display = 'none';
       notes.style.display = 'none';
    }

function revealLicensing() {
       var details = document.getElementById('details');
       var instances = document.getElementById('instances');
       var licensing = document.getElementById('licensing');
       var notes = document.getElementById('notes');
       details.style.display = 'none';
       instances.style.display = 'none';
       licensing.style.display = 'block';
       notes.style.display = 'none';
    }

function revealNotes() {
       var details = document.getElementById('details');
       var instances = document.getElementById('instances');
       var licensing = document.getElementById('licensing');
       var notes = document.getElementById('notes');
       details.style.display = 'none';
       instances.style.display = 'none';
       licensing.style.display = 'none';
       notes.style.display = 'block';
    }
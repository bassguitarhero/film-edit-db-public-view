from django.db import models
from time import time
from django.contrib.auth.models import User
from PIL import Image
from PIL import _imaging
from django.conf import settings
import os.path
from filmeditdb import settings
from django.core.files.storage import default_storage as storage


def get_upload_file_name(instance, filename):
    return settings.UPLOAD_FILE_PATTERN % (str(time()).replace('.','_'), filename)

# Create your models here.


class UserProfile(models.Model):
	user = models.OneToOneField(User, related_name='user_object')
	visible = models.BooleanField(default=True)
	bio = models.TextField(null=True, blank=True)
	avatar = models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	following = models.ManyToManyField(User, related_name='following_list')

	# save this
	def save(self, *args, **kwargs):
		super(UserProfile, self).save(*args, **kwargs)
		# resize this
		if self.avatar:
			size = 200, 200
			image = Image.open(self.avatar)
			image.thumbnail(size, Image.ANTIALIAS)
			fh = storage.open(self.avatar.name, "w")
			format = 'png'  # You need to set the correct image format here
			image.save(fh, format)
			fh.close()
		else:
			image = None
    
	def __unicode__(self):
		return self.user
    		
	def get_avatar(self):
		thumb = str(self.avatar)
		thumb = thumb.replace('assets/', '')

		return thumb
        
User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])
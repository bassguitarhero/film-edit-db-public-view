from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from forms import UserProfileForm
from django.contrib.auth.decorators import login_required
from docproject.models import Project, Asset, Access
from django.contrib.auth.models import User
from django.db.models import Q
from userprofile.models import UserProfile
from django.shortcuts import render


# Create your views here.

@login_required
def user_profile(request):
    if request.method == 'POST':
        form = UserProfileForm(request.POST, request.FILES, instance=request.user.profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/accounts/profile')

    else:
		user = request.user
		profile = user.profile
		form = UserProfileForm(instance=profile)
        
    thisuser = request.user
    if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser)).exists():
    	projects = Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser))
    else:
    	projects = None
    thisuser = request.user
    profile = thisuser.profile
    
            
    args = {'projects': projects, 'profile': profile, 'thisuser': thisuser}
    args.update(csrf(request))

    args['form'] = form

    return render_to_response('profile.html', args)

def view_profile(request, user_id=1):
	if User.objects.filter(id=user_id).exists():
		userlookup = User.objects.get(id=user_id)
		viewprofile = userlookup.profile
	else:
		userlookup = None
		viewprofile = None
	thisuser = request.user

	if not request.user.is_authenticated():
		followers = None
		loggedin = False
		projects = None
		following = None
	else:
		followers = UserProfile.objects.filter(following=thisuser).count()
		loggedin = 1
		if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser)).exists():
			projects = Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser))
		else:
			projects = None
		if UserProfile.objects.filter(user=thisuser, following=userlookup).exists():
			following = 1
		else:
			following = None

	next = request.path

	args = {'viewprofile': viewprofile, 'thisuser': thisuser, 'projects': projects, 'userlookup': userlookup, 'following': following, 'next': next, 'loggedin': loggedin, 'followers': followers}
	args.update(csrf(request))
    	
	return render_to_response('view_profile.html', args )

@login_required
def following(request, user_id=1):
	thisuser = request.user
	userlookup = User.objects.get(id=user_id)
	following = thisuser
	followers = UserProfile.objects.filter(following=thisuser).count()
	next = request.path
	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser)).exists():
		projects = Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser))
	else:
		projects = None

	args = {'following': following, 'thisuser': thisuser, 'userlookup': userlookup, 'projects': projects, 'followers': followers, 'next': next}
	args.update(csrf(request))

	return render_to_response('following.html', args)

@login_required
def followers(request, user_id=1):
	thisuser = request.user
	following = thisuser
	followers = UserProfile.objects.filter(following=request.user)
	followerscount = UserProfile.objects.filter(following=request.user).count()
	next = request.path

	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser)).exists():
		projects = 1
	else:
		projects = None

	args = {'thisuser': thisuser, 'projects': projects, 'followerscount': followerscount, 'next': next}
	args['following'] = request.user
	args['followers'] = []
	args.update(csrf(request))

	for f in followers:
		user_status = (f, request.user.user_object.following.filter(pk=f.pk).exists())
		args['followers'].append(user_status)

	return render_to_response('followers.html', args)
	
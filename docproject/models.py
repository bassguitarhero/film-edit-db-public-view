from django.db import models
from time import time
from django.contrib.auth.models import User
from PIL import Image
from PIL import _imaging
from django.conf import settings
from django.core.files.storage import default_storage as storage
from django.utils import timezone


def get_upload_file_name(instance, filename):
	return settings.UPLOAD_FILE_PATTERN % (str(time()).replace('.','_'), filename)

# Create your models here.


class Project(models.Model):
	name = models.CharField(max_length=200, default="My First Project")
	description = models.TextField(null=True, blank=True)
	thumbnail = models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	pubdate = models.DateTimeField(default=timezone.now)
	last_updated = models.DateTimeField(default=timezone.now)
	format = models.CharField(max_length=200, null=True, blank=True)
	region = models.CharField(max_length=200, null=True, blank=True)
	framerate = models.CharField(max_length=200, null=True, blank=True)
	filmtype = models.CharField(max_length=200, null=True, blank=True)
	length = models.CharField(max_length=200, null=True, blank=True)
	created_by = models.ForeignKey(User)


	def __unicode__(self):
		return self.name
        
	def save(self):
		if not self.id and not self.description:
			return
            
		super(Project, self).save()
		Access.objects.get_or_create(project=self)
		if self.thumbnail:
			size = 200, 200
			image = Image.open(self.thumbnail)
			image.thumbnail(size, Image.ANTIALIAS)
			fh = storage.open(self.thumbnail.name, "w")
			format = 'png'  # You need to set the correct image format here
			image.save(fh, format)
			fh.close()
            
	def get_project_thumbnail(self):
		thumb = str(self.thumbnail)
		if not settings.DEBUG:
			thumb = thumb.replace('assets/', '')
        
		return thumb

class Asset(models.Model):
	project = models.ForeignKey(Project)
	unique_id = models.IntegerField(default=0)
	thumbnail = models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	pubdate = models.DateTimeField(default=timezone.now)
	last_updated = models.DateTimeField(default=timezone.now)
	batch_id = models.CharField(max_length=200, null=True, blank=True)
	asset_type = models.CharField(max_length=200, null=True, blank=True)
	description = models.TextField(null=True, blank=True)
	used_in_film = models.BooleanField()
	master_status = models.CharField(max_length=200, null=True, blank=True)
	date_produced = models.DateField('date produced', null=True, blank=True)
	location = models.CharField(max_length=200, null=True, blank=True)
	file_location = models.CharField(max_length=200, null=True, blank=True)    
	footage = models.BooleanField()
	footage_format = models.CharField(max_length=200, null=True, blank=True)
	footage_region = models.CharField(max_length=200, null=True, blank=True)
	footage_type = models.CharField(max_length=200, null=True, blank=True)
	footage_fps = models.CharField(max_length=200, null=True, blank=True)
	footage_url = models.CharField(max_length=200, null=True, blank=True)
	footage_blackandwhite = models.BooleanField()
	footage_color = models.BooleanField()
	footage_sepia = models.BooleanField()
	stills = models.BooleanField()
	stills_credit = models.CharField(max_length=200, null=True, blank=True)
	stills_url = models.CharField(max_length=200, null=True, blank=True)
	stills_blackandwhite = models.BooleanField()
	stills_color = models.BooleanField()
	stills_sepia = models.BooleanField()
	music = models.BooleanField()
	music_format = models.CharField(max_length=200, null=True, blank=True)
	music_credit = models.CharField(max_length=200, null=True, blank=True)
	music_url = models.CharField(max_length=200, null=True, blank=True)
	license_obtained = models.BooleanField()
	license_type = models.CharField(max_length=200, null=True, blank=True)
	source = models.CharField(max_length=200, null=True, blank=True)
	source_contact = models.CharField(max_length=200, null=True, blank=True)
	source_email = models.CharField(max_length=200, null=True, blank=True)
	source_id = models.CharField(max_length=200, null=True, blank=True)
	source_phone = models.CharField(max_length=200, null=True, blank=True)
	source_fax = models.CharField(max_length=200, null=True, blank=True)
	source_address = models.TextField(null=True, blank=True)
	credit_language = models.CharField(max_length=200, null=True, blank=True)
	cost = models.CharField(max_length=200, null=True, blank=True)
	cost_model = models.CharField(max_length=200, null=True, blank=True)
	total_cost = models.CharField(max_length=200, null=True, blank=True)
	notes = models.TextField(null=True, blank=True)
	total_use = models.CharField(max_length=200, null=True, blank=True)
	timecode_total = models.CharField(max_length=200, null=True, blank=True)
	windowdub_total = models.CharField(max_length=200, null=True, blank=True)
	created_by = models.ForeignKey(User) 

	def __unicode__(self):
		return self.description
    
	def get_fields(self):
		return [(field.name, field.value_to_string(self)) for field in Asset._meta.fields]
    
	class Meta:
		ordering = ['-pubdate']
        
	def save(self):
		super(Asset, self).save()
		if self.thumbnail:
			size = 200, 200
			image = Image.open(self.thumbnail)
			image.thumbnail(size, Image.ANTIALIAS)
			fh = storage.open(self.thumbnail.name, "w")
			format = 'png'  # You need to set the correct image format here
			image.save(fh, format)
			fh.close()
                
	def get_thumbnail(self):
		thumb = str(self.thumbnail)
		if not settings.DEBUG:
			thumb = thumb.replace('assets/', '')
        
		return thumb

class Timecode(models.Model):
	asset = models.ForeignKey(Asset)
	project = models.IntegerField(default=0)
	created_by = models.ForeignKey(User)
	tc_unique = models.IntegerField(default=0)
	description = models.CharField(max_length=200, null=True, blank=True)
	film_tc_in = models.CharField(max_length=200, null=True, blank=True)
	film_tc_out = models.CharField(max_length=200, null=True, blank=True)
	film_tc_duration = models.CharField(max_length=200, null=True, blank=True)
	src_tc_in = models.CharField(max_length=200, null=True, blank=True)
	src_tc_out = models.CharField(max_length=200, null=True, blank=True)
	src_tc_duration = models.CharField(max_length=200, null=True, blank=True)
	src_wd_in = models.CharField(max_length=200, null=True, blank=True)
	src_wd_out = models.CharField(max_length=200, null=True, blank=True)
	src_wd_duration = models.CharField(max_length=200, null=True, blank=True)
	tc_pubdate = models.DateTimeField(default=timezone.now)
	last_updated = models.DateTimeField(default=timezone.now)

	def __unicode__(self):
		return self.description

class Access(models.Model):
    project = models.ForeignKey(Project, related_name='project_access')
    access_list = models.ManyToManyField(User)
    pubdate = models.DateTimeField(default=timezone.now)
    
    def __unicode__(self):
        return self.project.name

class AddAccess(models.Model):
     accessupdate = models.CharField(max_length=200, null=True, blank=True)
     accessremove = models.CharField(max_length=200, null=True, blank=True)
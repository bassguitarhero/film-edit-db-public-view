from django.contrib import admin
from docproject.models import Project, Asset, Timecode, Access

admin.site.register(Project)

admin.site.register(Asset)

admin.site.register(Timecode)

admin.site.register(Access)
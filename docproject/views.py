from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from docproject.models import Project, Asset, Timecode, Access, AddAccess
from django.http import HttpResponse
from forms import ProjectForm, ProjectUpdateForm, AssetForm, AssetUpdateForm, ImportForm, TimecodeForm, TimecodeUpdateForm, AddAccessForm
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.csrf import csrf_exempt
from django.template import Library
from django.db.models import Q
from django.utils import timezone
from datetime import datetime, timedelta
import csv
import json
from json import dumps, loads, JSONEncoder, JSONDecoder
from django.core.serializers.json import DjangoJSONEncoder

register = Library()


# Create your views here.

def home(request):
	thisuser = request.user
	if not request.user.is_authenticated():
		return render_to_response('home_nouser.html', {'user': thisuser })
	else:
		thisuser = request.user
		
		if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser)).exists():
			projects = Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser)).distinct('pubdate')
		else:
			projects = None
			
		args = {'projects': projects, 'thisuser': thisuser}
		args.update(csrf(request))

		return render_to_response('projects.html', args)

@login_required						
def new_project(request):
	thisuser = request.user
	
	if request.method == "POST":
		form = ProjectForm(request.POST, request.FILES)
		if form.is_valid():
			p = form.save(commit=False)
			p.created_by = thisuser
			form.save()
			
			return HttpResponseRedirect('/projects/all')
	else:
		form = ProjectForm()
		
		if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser)).exists():
			projects = 1
		else:
			projects = None
		
		args = {'projects': projects, 'thisuser': thisuser}
		args.update(csrf(request))
		
		args['form'] = form
		
		return render_to_response('new_project.html', args)

@login_required	
def projects(request):
	thisuser = request.user
	
	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser)).exists():
		projects = Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser)).distinct('pubdate')
	else:
		projects = None

	args = {'projects': projects, 'thisuser': thisuser}
	args.update(csrf(request))
		
	return render_to_response('projects.html', args)

@login_required									
def project(request, project_id=1):
	thisuser = request.user
	if Project.objects.filter(id=project_id).exists():
		project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
		accesslist = Access.objects.filter(project__id=project_id).order_by('pubdate')[0]
	else:
		project = None
		accesslist = None

	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
		permission = 1
	else:
		permission = None
		
	if Asset.objects.filter(project=project).exists():
		assets = 1
		lastasset = Asset.objects.filter(project=project).order_by('-pubdate')[0]
		lastassetid = lastasset.unique_id
	else:
		assets = None
		lastassetid = None

	args = {'permission': permission, 'project': project, 'thisuser': thisuser, 'assets': assets, 'lastassetid': lastassetid, 'accesslist': accesslist}
	args.update(csrf(request))
			
	return render_to_response('project.html', args)
								
@login_required						
def update_project(request, project_id):
	thisuser = request.user
	if Project.objects.filter(id=project_id).exists():
		project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
	else:
		project = None
	
	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
		permission = 1
	else:
		permission = None
		
	if Asset.objects.filter(project=project).exists():
		assets = 1
		lastasset = Asset.objects.filter(project=project).order_by('-pubdate')[0]
		lastassetid = lastasset.unique_id
	else:
		assets = None
		lastassetid = None
	
	if request.method == "POST":
		if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
			form = ProjectUpdateForm(request.POST, request.FILES)
			if form.is_valid():
				p = form.save(commit=False)
				p.id = project.id
				p.pubdate = project.pubdate
				p.created_by = project.created_by
				if p.thumbnail:
					pass
				else: p.thumbnail = project.thumbnail
				p.save()
				
				return HttpResponseRedirect('/projects/get/%s/' % project_id)
		else:
			return HttpResponseRedirect('/projects/get/%s/' % project_id)
			
	else:
		if project:
			form = ProjectUpdateForm(instance=project)
		else:
			form = None
		
		args = {'permission': permission, 'project': project, 'thisuser': thisuser, 'assets': assets, 'lastassetid': lastassetid}
		args.update(csrf(request))
		
		args['form'] = form
		
		return render_to_response('update_project.html', args)

@login_required									
def new_asset(request, project_id):
	thisuser = request.user
	if Project.objects.filter(id=project_id).exists():
		project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
	else:
		project = None
		
	if request.method == "POST":
		if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
			f = AssetForm(request.POST, request.FILES)
			if f.is_valid():
				e = f.save(commit=False)
				if e.date_produced:
					e.date_produced	= datetime.strptime(request.POST['date_produced'], '%m/%d/%Y')
				e.project = project
									
				if Asset.objects.filter(project=project).exists():
					latest = Asset.objects.filter(project=project).order_by('-pubdate')[0]
					asset_count = latest.unique_id if latest else 0
					asset_count += 1
				else:
					asset_count = 1
					
				e.unique_id = asset_count
				
				if e.footage_format or e.footage_region or e.footage_type or e.footage_fps or e.footage_url or e.footage_blackandwhite or e.footage_color or e.footage_sepia:
					e.footage = True
				
				if e.stills_credit or e.stills_url or e.stills_blackandwhite or e.stills_color or e.stills_sepia:
					e.stills = True
				
				if e.music_format or e.music_credit or e.music_url:
					e.music = True
					
				e.created_by = thisuser
							
				e.save()
					
				return HttpResponseRedirect('/projects/get/%s/assets/get/%s' % (project_id, asset_count))
		else:
			return HttpResponseRedirect('/projects/get/%s/assets/get/%s' % (project_id, asset_count))
				
	else:
			f = AssetForm()
	
	f = AssetForm()
	
	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
		permission = 1
	else:
		permission = None
		
	if Asset.objects.filter(project=project).exists():
		latestasset = Asset.objects.filter(project=project).order_by('-pubdate')[0]
		previous_asset = latestasset.unique_id if latestasset else 0
		newassetnumber = latestasset.unique_id if latestasset else 0
	else:
		newassetnumber = 0
		mostrecentassetnumber = 0
		previous_asset = 0
	
	newassetnumber += 1
				
	args = {'permission': permission, 'newassetnumber': newassetnumber, 'previous_asset': previous_asset, 'thisuser': thisuser, 'project': project}
	args.update(csrf(request))
		
	args['project'] = project
	args['form'] = f
	
	return render_to_response('new_asset.html',	args)

@login_required		
def assets(request, project_id=1):
	thisuser = request.user

	if Project.objects.filter(id=project_id).exists():
		project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
		asset_list = Asset.objects.filter(project=project).order_by('-pubdate')
		paginator = Paginator(asset_list, 20)

		page = request.GET.get('page')
		try:
			assets = paginator.page(page)
		except PageNotAnInteger:
			# If page is not an integer, deliver first page.
			assets = paginator.page(1)
		except EmptyPage:
			# If page is out of range (eg 9999), deliver last page of results.
			assets = paginator.page(paginator.num_pages)
	else:
		project = None
		assets = None

	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
		permission = 1
	else:
		permission = None

	args = {'project': project, 'assets': assets, 'thisuser': thisuser, 'permission': permission}
	args.update(csrf(request))
		
	return render_to_response('assets.html', args)

def getSec(s):
	t = s.split(':')
	return int(t[0]) * 3600 + int(t[1]) * 60 + int(t[2])

def getTimecode(seconds):
	hours = seconds // (60*60)
	seconds %= (60*60)
	minutes = seconds // 60
	seconds %= 60
	return "%02i:%02i:%02i" % (hours, minutes, seconds)
								
@login_required	
def asset(request, project_id=1, asset_id=1):
	thisuser = request.user
	if Project.objects.filter(id=project_id).exists():
		project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
	else:
		project = None

	if Asset.objects.filter(project=project, unique_id=asset_id).exists():
		asset = Asset.objects.get(project=project, unique_id=asset_id)
		current_asset_id = asset.unique_id
		previous_asset_id = current_asset_id
		previous_asset_id -= 1
 		if Asset.objects.filter(project=project, unique_id=previous_asset_id).exists():
			prev_asset = Asset.objects.get(project=project, unique_id=previous_asset_id)
		else:
			prev_asset = None
		next_asset_id = current_asset_id
		next_asset_id += 1
		if Asset.objects.filter(project=project, unique_id=next_asset_id).exists():
			next_asset = Asset.objects.get(project=project, unique_id=next_asset_id)
		else:
			next_asset = None
	else:
		asset = None
		prev_asset = None
		next_asset = None
		current_asset_id = None
	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
		permission = 1
	else:
		permission = None

	if Timecode.objects.filter(project=project_id, asset__unique_id=asset_id).exists():
		timecodes = Timecode.objects.filter(project=project_id, asset__unique_id=asset_id).order_by('tc_pubdate')
		timecode_duration = 0
		windowdub_duration = 0
		for timecode in timecodes:
			timecode_duration += int(getSec(timecode.src_tc_duration)) if timecode.src_tc_duration else 0
			windowdub_duration += int(getSec(timecode.src_wd_duration)) if timecode.src_wd_duration else 0
		timecode_show = getTimecode(timecode_duration)
		windowdub_show = getTimecode(windowdub_duration)
	else:
		timecodes = None
		timecode_duration = 0
		windowdub_duration = 0
		timecode_show = 0
		windowdub_show = 0

	args = {'permission': permission, 'project': project, 'asset': asset, 'thisuser': thisuser, 'prev_asset': prev_asset, 'next_asset': next_asset, 'timecodes': timecodes, 'timecode_show': timecode_show, 'windowdub_show': windowdub_show, 'timecode_duration': timecode_duration, 'windowdub_duration': windowdub_duration}
	args.update(csrf(request))
	
	return render_to_response('asset.html', args)
	
@login_required
def update_asset(request, project_id=1, asset_id=1):
	thisuser = request.user
	if Project.objects.filter(id=project_id).exists():
		project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
		asset = Asset.objects.filter(project=project, unique_id=asset_id).order_by('pubdate')[0]
	else:
		project = None
		asset = None
	
	if request.method == "POST":	
		if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
			form = AssetUpdateForm(request.POST, request.FILES)
			if form.is_valid():
				p = form.save(commit=False)
				p.id = asset.id
				p.project = asset.project
				p.pubdate = asset.pubdate
				if p.date_produced:
					p.date_produced	= datetime.strptime(request.POST['date_produced'], '%m/%d/%Y')
				p.unique_id = asset.unique_id
				if p.thumbnail:
					pass
				else: p.thumbnail = asset.thumbnail
				
				if p.footage_format or p.footage_region or p.footage_type or p.footage_fps or p.footage_url or p.footage_blackandwhite or p.footage_color or p.footage_sepia:
					p.footage = True
				
				if p.stills_credit or p.stills_url or p.stills_blackandwhite or p.stills_color or p.stills_sepia:
					p.stills = True
				
				if p.music_format or p.music_credit or p.music_url:
					p.music = True
					
				p.created_by = asset.created_by
				
				p.save()
				
				return HttpResponseRedirect('/projects/get/%s/assets/get/%s' % (project_id, asset_id))
		else:
			return HttpResponseRedirect('/projects/get/%s/assets/get/%s' % (project_id, asset_id))
	else:
		if asset:
			form = AssetUpdateForm(instance=asset)
		else:
			form = None
	
	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
		permission = 1
	else:
		permission = None
		
	args = {'asset': asset, 'permission': permission, 'thisuser': thisuser, 'project': project}
	args.update(csrf(request))
		
	args['form'] = form
	return render_to_response('update_asset.html', args)
	
@csrf_exempt
def search_projects(request):
	thisuser = request.user
	if request.method == "POST":
		search_text = request.POST['search_text']
	else:
		search_text = ''
		
	searchprojects = Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), description__contains=search_text).distinct('pubdate')
		
	return render_to_response('ajax_search_projects.html', {'searchprojects': searchprojects})
	
@csrf_exempt
def search_assets(request, project_id=1):
	thisuser = request.user
	if request.method == "POST":
		search_text = request.POST['search_text']
	else:
		search_text = ''
		
	searchassets = Asset.objects.filter(description__contains=search_text, project__id=project_id).distinct('pubdate')
	project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
		
	return render_to_response('ajax_search_assets.html', {'searchassets': searchassets, 'project': project})
	
@register.filter
def is_false(arg):
	return arg is False

@login_required
def sort(request, project_id=1):
	thisuser = request.user
	if Project.objects.filter(id=project_id).exists():
		project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
	else:
		project = None
		
	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
		permission = 1
	else:
		permission = None
		
	if Asset.objects.filter(project__id=project_id, unique_id=1):
		assets = 1
	else:
		assets = None
		
	
	if request.POST:
		if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
			if request.POST.get('date_start') and request.POST.get('date_end'):
				date_start 	= datetime.strptime(request.POST['date_start'], '%m/%d/%Y')
				date_end 	= datetime.strptime(request.POST['date_end'], '%m/%d/%Y')
				q_date		= Q(date_produced__range=[date_start, date_end])
			else:
				q_date		= Q(date_produced__isnull=False) | Q(date_produced__isnull=True)
			
			text_fields = {
				'asset_type': request.POST.get('asset_type'),
				'description': request.POST.get('description'),
				'master_status': request.POST.get('master_status'),
				'location': request.POST.get('location'),
				'file_location': request.POST.get('file_location'),
				'footage_format': request.POST.get('footage_format'),
				'footage_region': request.POST.get('footage_region'),
				'footage_type': request.POST.get('footage_type'),
				'footage_fps': request.POST.get('footage_fps'),
				'footage_url': request.POST.get('footage_url'),
				'stills_credit': request.POST.get('stills_credit'),
				'stills_url': request.POST.get('stills_url'),
				'music_format': request.POST.get('music_format'),
				'music_credit': request.POST.get('music_credit'),
				'music_url': request.POST.get('music_url'),
				'license_type': request.POST.get('license_type'),
				'source': request.POST.get('source'),
				'source_contact': request.POST.get('source_contact'),
				'source_email': request.POST.get('source_email'),
				'source_id': request.POST.get('source_id'),
				'source_phone': request.POST.get('source_phone'),
				'source_fax': request.POST.get('source_fax'),
				'source_address': request.POST.get('source_address'),
				'credit_language': request.POST.get('source_language'),
				'cost': request.POST.get('cost'),
				'cost_model': request.POST.get('cost_model'),
				'total_cost': request.POST.get('total_cost'),
				'notes': request.POST.get('notes')
				}
				
			boolean_fields = {
				'used_in_film': request.POST.get('used_in_film'),
				'footage_blackandwhite': request.POST.get('footage_blackandwhite'),
				'footage_color': request.POST.get('footage_color'),
				'footage_sepia': request.POST.get('footage_sepia'),
				'stills_blackandwhite': request.POST.get('stills_blackandwhite'),
				'stills_color': request.POST.get('stills_color'),
				'stills_sepia': request.POST.get('stills_sepia'),
				'license_obtained': request.POST.get('license_obtained')
				}
						
			q_objects = Q()
			
			for field, value in text_fields.iteritems():
				if value:
					q_objects = Q(**{field+'__contains': value})
					
			q_boolean = Q()
					
			for field, value in boolean_fields.iteritems():
				if value:
					q_boolean |= Q(**{field: True})
					
			query_results = Asset.objects.values('id', 'unique_id', 'asset_type', 'description', 'used_in_film', 'master_status', 
				'date_produced', 'location', 'file_location', 'footage', 'footage_format', 'footage_region', 'footage_type', 
				'footage_fps', 'footage_url', 'footage_blackandwhite', 'footage_color', 'footage_sepia', 'stills', 'stills_credit', 
				'stills_url', 'stills_blackandwhite', 'stills_color', 'stills_sepia', 'music', 'music_format', 'music_credit', 
				'music_url', 'license_obtained', 'license_type', 'source', 'source_contact', 'source_email', 'source_id', 
				'source_phone', 'source_fax', 'source_address', 'credit_language', 'total_use', 'timecode_total', 'windowdub_total', 
				'cost', 'cost_model', 'total_cost', 'notes').filter(q_date, q_objects, q_boolean, project__id=project_id)

			for instance in query_results:
				if instance['date_produced']:
					instance['date_produced'] = instance['date_produced'].strftime('%Y-%m-%d')

			request.session['query_results'] = list(query_results)

			args = {'query_results': query_results, 'thisuser': thisuser, 'project': project, 'assets': assets, 'permission': permission}
			args.update(csrf(request))
			
			args['query_results'] = query_results

			return render_to_response('sort_results.html', args)
		else:
			return render_to_response('sort_results.html', args)
			
	else:
		
		args = {'thisuser': thisuser, 'project': project, 'assets': assets, 'permission': permission}
		args.update(csrf(request))
	
		return render_to_response('sort.html', args)
		
def export_csv(request, project_id=1):
	thisuser = request.user
	if Project.objects.filter(id=project_id).exists():
		project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
	else:
		project = None

	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
		response = HttpResponse(content_type='text/csv')
		response['Content-Disposition'] = 'attachment; filename="Your Database.csv"'

		export_data = Asset.objects.filter(project__id=project_id)

		writer = csv.writer(response)
		writer.writerow(['Project', 'Asset ID', 'Asset Type', 'Description', 'In Film', 'Master Status',
				'Date Prduced', 'Location Produced', 'File Address', 'Footage', 'Footage Format', 'Footage Region',
				'Footage Type', 'Footage FPS', 'Footage URL', 'Black & White', 'Color', 'Sepia', 'Stills', 'Stills Credit',
				'Stills URL', 'Black & White', 'Color', 'Sepia', 'Music', 'Music Format', 'Music Credit', 'Music URL',
				'License Obtained', 'License Type', 'Source', 'Contact', 'Email', 'Reference ID', 'Phone', 'Fax', 'Address',
				'Credit Language', 'Total Project Timecode', 'Total Source Timecode', 'Total Windowdub', 'Cost', 'Cost Model', 'Total Cost', 'Notes',
				'Timecode Description', 'Project TC IN', 'Project TC OUT', 'Project TC Duration', 'SRC TC IN', 'SRC TC OUT', 'SRC TC Duration', 
				'SRC WD IN', 'SRC WD OUT', 'SRC WD Duration' ])
		for obj in export_data:
			writer.writerow ( [obj.project.id, obj.unique_id, obj.asset_type, obj.description, obj.used_in_film, obj.master_status,
				obj.date_produced, obj.location, obj.file_location, obj.footage, obj.footage_format, obj.footage_region, obj.footage_type,
				obj.footage_fps, obj.footage_url, obj.footage_blackandwhite, obj.footage_color, obj.footage_sepia, obj.stills, obj.stills_credit,
				obj.stills_url, obj.stills_blackandwhite, obj.stills_color, obj.stills_sepia, obj.music, obj.music_format, obj.music_credit,
				obj.music_url, obj.license_obtained, obj.license_type, obj.source, obj.source_contact, obj.source_email, obj.source_id,
				obj.source_phone, obj.source_fax, obj.source_address, obj.credit_language, obj.total_use, obj.timecode_total, obj.windowdub_total, 
				obj.cost, obj.cost_model, obj.total_cost, obj.notes] )
			for tc in obj.timecode_set.all():
				writer.writerow ( [None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,
					None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,
					tc.description, tc.film_tc_in, tc.film_tc_out, tc.film_tc_duration, tc.src_tc_in, tc.src_tc_out, tc.src_tc_duration,
					tc.src_wd_in, tc.src_wd_out, tc.src_wd_duration	])
	        
	    	return response

	else:
		return HttpResponseRedirect('/projects/all')
    	
def export_results(request, project_id=1):
	thisuser = request.user
	if Project.objects.filter(id=project_id).exists():
		project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
	else:
		project = None
	if request.POST:
		if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
			response = HttpResponse(content_type='text/csv')
			response['Content-Disposition'] = 'attachment; filename="Your Database.csv"'

			export_data = request.session['query_results']
			query_results = Asset.objects.filter(id__in=[instance['id'] for instance in export_data]).order_by('pubdate')

			writer = csv.writer(response)
			writer.writerow(['Project', 'Asset ID', 'Asset Type', 'Description', 'In Film', 'Master Status',
				'Date Produced', 'Location Produced', 'File Address', 'Footage', 'Footage Format', 'Footage Region',
				'Footage Type', 'Footage FPS', 'Footage URL', 'Black & White', 'Color', 'Sepia', 'Stills', 'Stills Credit',
				'Stills URL', 'Black & White', 'Color', 'Sepia', 'Music', 'Music Format', 'Music Credit', 'Music URL',
				'License Obtained', 'License Type', 'Source', 'Contact', 'Email', 'Reference ID', 'Phone', 'Fax', 'Address',
				'Credit Language', 'Total Project Timecode', 'Total Source Timecode', 'Total Windowdub', 'Cost', 'Cost Model', 'Total Cost', 'Notes',
				'Timecode Description', 'Project TC IN', 'Project TC OUT', 'Project TC Duration', 'SRC TC IN', 'SRC TC OUT', 'SRC TC Duration',
				'SRC WD IN', 'SRC WD OUT', 'SRC WD Duration' ])
			for obj in query_results:
				writer.writerow ( [project.id, obj.unique_id, obj.asset_type, obj.description, obj.used_in_film, obj.master_status,
					obj.date_produced, obj.location, obj.file_location, obj.footage, obj.footage_format, obj.footage_region, 
					obj.footage_type, obj.footage_fps, obj.footage_url, obj.footage_blackandwhite, obj.footage_color, obj.footage_sepia, 
					obj.stills, obj.stills_credit, obj.stills_url, obj.stills_blackandwhite, obj.stills_color, obj.stills_sepia, 
					obj.music, obj.music_format, obj.music_credit, obj.music_url, obj.license_obtained, obj.license_type, obj.source, 
					obj.source_contact, obj.source_email, obj.source_id, obj.source_phone, obj.source_fax, obj.source_address, 
					obj.credit_language, obj.total_use, obj.timecode_total, obj.windowdub_total, obj.cost, obj.cost_model, 
					obj.total_cost, obj.notes ] )
				for tc in obj.timecode_set.all():
					writer.writerow ( [None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,
						None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,
						tc.description, tc.film_tc_in, tc.film_tc_out, tc.film_tc_duration, tc.src_tc_in, tc.src_tc_out, tc.src_tc_duration,
						tc.src_wd_in, tc.src_wd_out, tc.src_wd_duration	])
		        
		    	return response
		else:
			return HttpResponseRedirect('/projects/all')
   
@staff_member_required 	
def import_csv(request, project_id=1):
	thisuser = request.user
	if Project.objects.filter(id=project_id).exists():
		project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
	else:
		project = None
	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
		permission = 1
	else:
		permission = None
	
	if request.method == "POST":
		if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
			form = ImportForm(request.POST, request.FILES)
			if form.is_valid():
				imports = csv.reader(request.FILES['import_csv'])
				next(imports)
				for line in imports:
					if line:
						import_data = Entry()
						import_data.batch_id = line[0]
						import_data.asset_type = line[1]
						import_data.description = line[2]
						import_data.used_in_film = line[3]
						import_data.master_status = line[4]
						import_data.date_produced = datetime.strptime(line[5], '%m/%d/%Y')
						import_data.location = line[6]
						import_data.file_location = line[7]
						import_data.footage_format = line[8]
						import_data.footage_region = line[9]
						import_data.footage_type = line[10]
						import_data.footage_fps = line[11]
						import_data.footage_url = line[12]
						import_data.footage_blackandwhite = line[13]
						import_data.footage_color = line[14]
						import_data.footage_sepia = line[15]
						import_data.stills_credit = line[16]
						import_data.stills_url = line[17]
						import_data.stills_blackandwhite = line[18]
						import_data.stills_color = line[19]
						import_data.stills_sepia = line[20]
						import_data.music_format = line[21]
						import_data.music_credit = line[22]
						import_data.music_url = line[23]
						import_data.license_obtained = line[24]
						import_data.license_type = line[25]
						import_data.source = line[26]
						import_data.source_contact = line[27]
						import_data.source_email = line[28]
						import_data.source_id = line[29]
						import_data.soure_phone = line[30]
						import_data.source_fax = line[31]
						import_data.source_address = line[32]
						import_data.credit_language = line[33]
						import_data.cost = line[39]
						import_data.cost_model = line[40]
						import_data.total_cost = line[41]
						import_data.notes = line[42]
						import_data.project_id = project
						import_data.created_by = thisuser
						if Asset.objects.filter(project_id=project_id):
							latest = Asset.objects.filter(project=project).order_by('-pubdate')[0]
							current = latest.unique_id if latest else 0
							current += 1
							import_data.unique_id = current
						else:
							import_data.unique_id = 1
						import_data.save()

				args = {'project': project, 'permission': permission}
				args.update(csrf(request))	

				return render_to_response('imported.html', {'project': project, 'permission': permission})
		else:
			return HttpResponseRedirect('/projects/all')
	
	else:
		form = ImportForm()
		
	args = {'thisuser': thisuser, 'project': project, 'permission': permission}
	args.update(csrf(request))
	
	args['form'] = form
	
	return render_to_response('import.html', args)

@login_required
def timecode(request, project_id=1, asset_id=1):
	if Project.objects.filter(id=project_id).exists():
		project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
		asset = Asset.objects.filter(project=project, unique_id=asset_id).order_by('pubdate')[0]
	else:
		project = None
		asset = None
	thisuser = request.user

	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
		permission = 1
	else:
		permission = None

	if request.method == "POST":
		if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
			t = TimecodeForm(request.POST)
			if t.is_valid():
				s = t.save(commit=False)
				s.project = project.id
				s.asset = asset
				s.created_by = thisuser

				if Timecode.objects.filter(project=project_id, asset__unique_id=asset_id).exists():
					latest = Timecode.objects.filter(project=project_id, asset__unique_id=asset_id).order_by('-tc_pubdate')[0]
					tc_count = latest.tc_unique if latest else 0
					tc_count += 1
				else:
					tc_count = 1
					
				s.tc_unique = tc_count

				s.save()

				return HttpResponseRedirect('/projects/get/%s/assets/get/%s' % (project_id, asset_id))
		else:
			return HttpResponseRedirect('/projects/all')

	else:
		t = TimecodeForm()

	args = {'project': project, 'asset': asset, 'permission': permission, 'thisuser': thisuser}
	args.update(csrf(request))

	args['project'] = project
	args['asset'] = asset
	args['form'] = t

	return HttpResponseRedirect('/projects/get/%s/assets/get/%s' % (project_id, asset_id))

@login_required
def update_timecode(request, project_id=1, asset_id=1, timecode_id=1):
	if Project.objects.filter(id=project_id).exists():
		project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
		asset = Asset.objects.get(project_id=project_id, unique_id=asset_id)
		timecode = Timecode.objects.get(project=project_id, asset__unique_id=asset_id, tc_unique=timecode_id)
	else:
		project = None
		asset = None
		timecode = None
	thisuser = request.user

	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
		permission = 1
	else:
		permission = None

	if request.method == "POST":
		if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
			t = TimecodeUpdateForm(request.POST)
			if t.is_valid():
				s = t.save(commit=False)
				s.id = timecode.id
				s.project = project.id
				s.asset = asset
				s.created_by = thisuser
				s.tc_unique = timecode.tc_unique
				s.tc_pubdate = timecode.tc_pubdate	
				
				s.save()

				return HttpResponseRedirect('/projects/get/%s/assets/get/%s' % (project_id, asset_id))
		else:
			return HttpResponseRedirect('/projects/all')

	else:
		t = TimecodeUpdateForm(instance = timecode)

	args = {'project': project, 'asset': asset, 'permission': permission, 'thisuser': thisuser, 'timecode': timecode}
	args.update(csrf(request))

	args['project'] = project
	args['asset'] = asset
	args['timecode'] = timecode
	args['form'] = t

	return render_to_response('update_timecode.html', args)

@login_required	
def access(request, project_id=1):
	thisuser = request.user
	if Project.objects.filter(id=project_id).exists():
		project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
		accesslist = Access.objects.filter(project__id=project_id).order_by('pubdate')[0]
	else:
		project = None
		accesslist = None

	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
		permission = 1
	else:
		permission = None

	if Asset.objects.filter(project=project).exists():
		assets = 1
	else:
		assets = None

	if request.method == "POST":
		if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
			form = AddAccessForm(request.POST)
			if form.is_valid():
				p = form.save(commit=False)
				
				adduserfromform = p.accessupdate
				if User.objects.filter(username=adduserfromform).exists():
					usertoadd = User.objects.get(username=adduserfromform)
					projecttoadd = Access.objects.get(project__id=project_id)
					projecttoadd.access_list.add(usertoadd)
				else:
					usertoadd = None
					
				form.save()
				
				return HttpResponseRedirect('/projects/get/%s/access' % project_id)
		else:
			return HttpResponseRedirect('/projects/all')
	else:
		form = AddAccessForm()
		
		args = {'project': project, 'permission': permission, 'thisuser': thisuser, 'assets': assets, 'accesslist': accesslist}
    	args.update(csrf(request))
    	
    	args['form'] = form
    
	return render_to_response('access.html', args)

@login_required
def remove_access(request, project_id=1, user_id=1):
	if Project.objects.filter(id=project_id).exists():
		project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
	else:
		project = None

	thisuser = request.user

	if User.objects.get(id=user_id).exists():
		usertoremove = User.objects.get(id=user_id)
	else:
		usertoremove = None

	if Project.objects.filter(created_by=thisuser, id=project_id).exists():
		if Access.objects.filter(project__id=project_id, access_list=usertoremove).exists():
			if User.objects.filter(username=usertoremove).exists():
				projecttoremove = Access.objects.get(project__id=project_id)
				projecttoremove.access_list.remove(usertoremove)
			else:
				usertoremove = None
		else:
			return HttpResponseRedirect('/projects/get/%s/access' % project_id)

	return HttpResponseRedirect('/projects/get/%s/access' % project_id)

@login_required
def save_timecode(request, project_id=1, asset_id=1):
	thisuser = request.user
	if Project.objects.filter(id=project_id).exists():
		project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
		asset = Asset.objects.get(project=project, unique_id=asset_id)
	else:
		project = None
		asset = None
	
	if request.method == "POST":
		if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
			form = AssetForm(request.POST)
			if form.is_valid():
				p = form.save(commit=False)
				p.id = asset.id
				p.project = asset.project
				p.unique_id = asset.unique_id
				p.pubdate = asset.pubdate
				p.batch_id = asset.batch_id
				p.date_produced	= asset.date_produced
				p.asset_type = asset.asset_type
				p.thumbnail = asset.thumbnail
				p.description = asset.description
				p.location = asset.location
				p.file_location = asset.file_location
				p.footage = asset.footage
				p.footage_format = asset.footage_format
				p.footage_region = asset.footage_region
				p.footage_type = asset.footage_type
				p.footage_fps = asset.footage_fps
				p.footage_url = asset.footage_url
				p.footage_blackandwhite = asset.footage_blackandwhite
				p.footage_color = asset.footage_sepia
				p.footage_sepia = asset.footage_sepia
				p.stills = asset.stills
				p.stills_credit = asset.stills_credit
				p.stills_url = asset.stills_url
				p.stills_blackandwhite = asset.stills_blackandwhite
				p.stills_color = asset.stills_color
				p.stills_sepia = asset.stills_sepia
				p.music = asset.music
				p.music_format = asset.music_format
				p.music_credit = asset.music_credit
				p.music_url = asset.music_url
				p.license_obtained = asset.license_obtained
				p.license_type = asset.license_type
				p.source = asset.source
				p.source_contact = asset.source_contact
				p.source_email = asset.source_email
				p.source_id = asset.source_id
				p.source_phone = asset.source_phone
				p.source_fax = asset.source_fax
				p.source_address = asset.source_address
				p.credit_language = asset.credit_language
				p.cost = asset.cost
				p.cost_model = asset.cost_model
				p.notes = asset.notes				
				p.created_by = asset.created_by
				
				p.save()
				
				return HttpResponseRedirect('/projects/get/%s/assets/get/%s' % (project_id, asset_id))
		else:
			return HttpResponseRedirect('/projects/all')
	else:
		form = AssetForm(instance=asset)
	
	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
		permission = 1
	else:
		permission = None
		
	args = {'asset': asset, 'permission': permission, 'thisuser': thisuser, 'project': project}
	args.update(csrf(request))
		
	args['form'] = form
	return render_to_response('update_asset.html', args)

@login_required
def delete_timecode(request, project_id=1, asset_id=1, timecode_id=1):
	if Project.objects.filter(id=project_id).exists():
		project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
		asset = Asset.objects.get(project_id=project_id, unique_id=asset_id)
		timecode = Timecode.objects.get(project=project_id, asset__unique_id=asset_id, tc_unique=timecode_id)
	else:
		project = None
		asset = None
		timecode = None
	thisuser = request.user

	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
		permission = 1
	else:
		permission = None

	if timecode:
		Timecode.objects.get(project=project_id, asset__unique_id=asset_id, tc_unique=timecode_id).delete()

	return HttpResponseRedirect('/projects/get/%s/assets/get/%s' % (project_id, asset_id))

@login_required
def delete_asset(request, project_id=1, asset_id=1):
	if Project.objects.filter(id=project_id).exists():
		project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
		asset = Asset.objects.get(project_id=project_id, unique_id=asset_id)
	else:
		project = None
		asset = None
	thisuser = request.user

	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
		if asset:
			Asset.objects.get(project__id=project_id, unique_id=asset_id).delete()

	return HttpResponseRedirect('/projects/get/%s/' % project_id)

@login_required
def delete_project(request, project_id=1, asset_id=1):
	if Project.objects.filter(id=project_id).exists():
		project = Project.objects.filter(id=project_id).order_by('pubdate')[0]
	else:
		project = None
	thisuser = request.user

	if Project.objects.filter(Q(created_by=thisuser) | Q(project_access__access_list=thisuser), id=project_id).exists():
		Project.objects.get(id=project_id).delete()

	return HttpResponseRedirect('/projects/all/')
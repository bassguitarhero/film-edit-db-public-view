from django import forms
from models import Project, Asset, Timecode, Access, AddAccess
from django.contrib.auth.models import User

class ProjectForm(forms.ModelForm):

    class Meta:
        model = Project
        fields = ('name', 'description', 'thumbnail', 'format', 'region', 'framerate', 'filmtype', 'length')

def __init__(self, *args, **kwargs):
        self._user = kwargs.pop('user', None)
        super(ProjectsForm, self).__init__(*args, **kwargs)
        
class ProjectUpdateForm(forms.ModelForm):

    class Meta:
        model = Project
        fields = ('name', 'description', 'thumbnail', 'format', 'region', 'framerate', 'filmtype', 'length')
        widgets = {
			'thumbnail': forms.FileInput()
		}

class AssetForm(forms.ModelForm):

    class Meta:
        model = Asset
        fields = ('asset_type', 'thumbnail', 'description', 'used_in_film', 'master_status', 'date_produced', 'location', 'file_location', 'footage', 'footage_format', 'footage_region', 'footage_type', 'footage_fps', 'footage_url', 'footage_blackandwhite', 'footage_color', 'footage_sepia', 'stills', 'stills_credit', 'stills_url', 'stills_blackandwhite', 'stills_color', 'stills_sepia', 'music', 'music_format', 'music_credit', 'music_url', 'license_obtained', 'license_type', 'source', 'source_contact', 'source_email', 'source_id', 'source_phone', 'source_fax', 'source_address', 'credit_language', 'cost', 'cost_model', 'total_cost', 'notes', 'timecode_total', 'windowdub_total')
        widgets = {
        	'description': forms.Textarea(attrs={'rows': '3'}),
        	'date_produced': forms.TextInput(attrs={'size': '40'}),
        	'location': forms.TextInput(attrs={'size': '40'}),
        	'file_location': forms.TextInput(attrs={'size': '40'}),
        	'footage_format': forms.TextInput(attrs={'size': '60'}),
        	'footage_url': forms.TextInput(attrs={'size': '60'}),
        	'stills_credit': forms.TextInput(attrs={'size': '60'}),
        	'stills_url': forms.TextInput(attrs={'size': '60'}),
        	'music_credit': forms.TextInput(attrs={'size': '60'}),
        	'music_url': forms.TextInput(attrs={'size': '60'}),
        	'license_type': forms.TextInput(attrs={'size': '40'}),
        	'source': forms.TextInput(attrs={'size': '25'}),
            'source_id': forms.TextInput(attrs={'size': '25'}),
        	'source_contact': forms.TextInput(attrs={'size': '25'}),
        	'source_email': forms.TextInput(attrs={'size': '25'}),
            'source_phone': forms.TextInput(attrs={'size': '25'}),
            'source_fax': forms.TextInput(attrs={'size': '25'}),
        	'source_address': forms.Textarea(attrs={'rows': '4',  'cols': '30'}),
        	'credit_language': forms.TextInput(attrs={'size': '60'}),
			'cost': forms.TextInput(attrs={'size': '10'}),
			'total_cost': forms.TextInput(attrs={'size': '10'}),
			'notes': forms.Textarea(attrs={'rows': '4', 'cols': '60'})
        }
        
def __init__(self, *args, **kwargs):
        self.entry_unique = kwargs.pop('entry_unique', None)
        super(EntriesForm, self).__init__(*args, **kwargs)

def save(self, commit=True):
        inst = super(EntriesForm, self).save(commit=False)
        inst.entry_unique = self.entry_unique
        if commit:
            inst.save()
            self.save_m2m()
        return inst
        
class AssetUpdateForm(forms.ModelForm):

    class Meta:
        model = Asset
        fields = ('asset_type', 'thumbnail', 'description', 'used_in_film', 'master_status', 'date_produced', 'location', 'file_location', 'footage', 'footage_format', 'footage_region', 'footage_type', 'footage_fps', 'footage_url', 'footage_blackandwhite', 'footage_color', 'footage_sepia', 'stills', 'stills_credit', 'stills_url', 'stills_blackandwhite', 'stills_color', 'stills_sepia', 'music', 'music_format', 'music_credit', 'music_url', 'license_obtained', 'license_type', 'source', 'source_contact', 'source_email', 'source_id', 'source_phone', 'source_fax', 'source_address', 'credit_language', 'cost', 'cost_model', 'total_cost', 'notes', 'timecode_total', 'windowdub_total')
        widgets = {
        	'description': forms.Textarea(attrs={'rows': '3'}),
            'date_produced': forms.TextInput(attrs={'size': '40'}),
            'location': forms.TextInput(attrs={'size': '40'}),
            'file_location': forms.TextInput(attrs={'size': '40'}),
            'footage_format': forms.TextInput(attrs={'size': '60'}),
            'footage_url': forms.TextInput(attrs={'size': '60'}),
            'stills_credit': forms.TextInput(attrs={'size': '60'}),
            'stills_url': forms.TextInput(attrs={'size': '60'}),
            'music_credit': forms.TextInput(attrs={'size': '60'}),
            'music_url': forms.TextInput(attrs={'size': '60'}),
            'license_type': forms.TextInput(attrs={'size': '40'}),
            'source': forms.TextInput(attrs={'size': '25'}),
            'source_id': forms.TextInput(attrs={'size': '25'}),
            'source_contact': forms.TextInput(attrs={'size': '25'}),
            'source_email': forms.TextInput(attrs={'size': '25'}),
            'source_phone': forms.TextInput(attrs={'size': '25'}),
            'source_fax': forms.TextInput(attrs={'size': '25'}),
            'source_address': forms.Textarea(attrs={'rows': '4',  'cols': '30'}),
            'credit_language': forms.TextInput(attrs={'size': '60'}),
            'cost': forms.TextInput(attrs={'size': '10'}),
            'total_cost': forms.TextInput(attrs={'size': '10'}),
            'notes': forms.Textarea(attrs={'rows': '10', 'cols': '60'}),
			'thumbnail': forms.FileInput(),
        	}
        	
class ImportForm(forms.Form):
	import_csv = forms.FileField()

class TimecodeForm(forms.ModelForm):
    class Meta:
        model = Timecode
        fields = ('description', 'film_tc_in', 'film_tc_out', 'film_tc_duration', 'src_tc_in', 'src_tc_out', 'src_tc_duration', 'src_wd_in', 'src_wd_out', 'src_wd_duration')

class TimecodeUpdateForm(forms.ModelForm):
    class Meta:
        model = Timecode
        fields = ('description', 'film_tc_in', 'film_tc_out', 'film_tc_duration', 'src_tc_in', 'src_tc_out', 'src_tc_duration', 'src_wd_in', 'src_wd_out', 'src_wd_duration')

class AddAccessForm(forms.ModelForm):
    class Meta:
        model = AddAccess
        fields = ('accessupdate',)
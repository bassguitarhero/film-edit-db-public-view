from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^$', 'lobby.views.home'),
    url(r'^all/$', 'lobby.views.home'),
    url(r'^new_post/$', 'lobby.views.new_post'),
    url(r'^posts/get/(?P<post_id>\d+)/$', 'lobby.views.post'),
    url(r'^posts/get/(?P<post_id>\d+)/like/$', 'lobby.views.like_post'),
    url(r'^posts/get/(?P<post_id>\d+)/edit/$', 'lobby.views.edit_post'),
    url(r'^posts/get/(?P<post_id>\d+)/new_comment/$', 'lobby.views.new_comment'),
    url(r'^posts/get/(?P<post_id>\d+)/comments/(?P<comment_id>\d+)/like/$', 'lobby.views.like_comment'),
    url(r'^posts/get/(?P<post_id>\d+)/comments/(?P<comment_id>\d+)/edit/$', 'lobby.views.edit_comment'),
    url(r'^posts/category/(?P<category>\w+)/$', 'lobby.views.category'),
    url(r'^posts/tag/(?P<tag>[a-zA-Z0-9_.-]+)/$', 'lobby.views.tag'),
    url(r'^posts/search/$', 'lobby.views.search_posts'),
    )

from django.contrib import admin
from lobby.models import Post, Comment

admin.site.register(Post)

admin.site.register(Comment)
from django import template

register = template.Library()

@register.filter
def dashspace (string):
	
	return string.replace('-', ' ')
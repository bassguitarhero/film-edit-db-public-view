from django.db import models
from time import time
from django.contrib.auth.models import User
from PIL import Image
from PIL import _imaging
from django.conf import settings
from django.core.files.storage import default_storage as storage
from django.utils import timezone
from django.shortcuts import render_to_response
import re

# Create your models here.

def urlify(s):

     # Remove all non-word characters (everything except numbers and letters)
     s = re.sub(r"[^\w\s]", '', s)

     # Replace all runs of whitespace with a single dash
     s = re.sub(r"\s+", '-', s)

     return s

class Post(models.Model):
	created_by = models.ForeignKey(User, related_name='post_created_by')
	pubdate = models.DateTimeField(default=timezone.now)
	title = models.CharField(max_length=200, null=True, blank=True)
	body = models.TextField(null=True, blank=True)
	category = models.CharField(max_length=200, null=True, blank=True)
	tags = models.CharField(max_length=200, null=True, blank=True)
	last_updated = models.DateTimeField(default=timezone.now)
	likes = models.ManyToManyField(User, related_name='post_likes')

	def __unicode__(self):
		return self.title

	def split_tags(self):
		return [urlify(tag) for tag in self.tags.split(', ')]
		#alltags =  self.tags.split(', ')
		#for tag in range(len(alltags)):
		#	urlify(alltags[tag])
		#return alltags
		
	class Meta:
		ordering = ['-pubdate']

class Comment(models.Model):
	created_by = models.ForeignKey(User, related_name='comment_created_by')
	pubdate = models.DateTimeField(default=timezone.now)
	id_unique = models.IntegerField(default=0)
	reply_to = models.ForeignKey(Post, related_name='post_replied_to')
	pubdate = models.DateTimeField(default=timezone.now)
	comment_text = models.TextField(null=True, blank=True)
	last_updated = models.DateTimeField(default=timezone.now)
	likes = models.ManyToManyField(User, related_name='comment_likes')

	def __unicode__(self):
		return self.comment_text

	class Meta:
		ordering = ['pubdate']